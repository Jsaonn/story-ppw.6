from django.urls import path
from . import views

app_name = "kegiatanApp"

urlpatterns = [
	path('kegiatan/', views.home, name = 'home'),
]
