from django import forms
from .models import Kegiatan, Orang

class CreateKegiatanForm(forms.Form):
    nama_kegiatan = forms.CharField(label='Nama Kegiatan ', max_length=30, required=True,
    widget=forms.TextInput(attrs={'class' : 'form-control'})
    )

class CreateOrangForm(forms.Form):
    nama_orang = forms.CharField(label='Nama Peserta ', max_length=50, required=True,
    widget=forms.TextInput(attrs={'class' : 'form-control'})
    )