from django.apps import AppConfig


class KegiatanappConfig(AppConfig):
    name = 'kegiatanApp'
