from django.shortcuts import render, redirect
from . import forms, models
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseBadRequest


# Create your views here.
@csrf_exempt

def home(request):
    if request.method == "POST":
        if 'nama_kegiatan' in request.POST:
            formKegiatan = forms.CreateKegiatanForm(request.POST)
            if formKegiatan.is_valid():
                temp = models.Kegiatan()
                temp.nama_kegiatan = formKegiatan.cleaned_data['nama_kegiatan']
                temp.save()
        elif 'nama_orang' in request.POST and 'id_kegiatan' in request.POST:
            formOrang = forms.CreateOrangForm(request.POST)
            if formOrang.is_valid():
                models.Kegiatan.objects.get(id=request.POST['id_kegiatan'])
                temp = models.Orang()
                temp.nama_orang = formOrang.cleaned_data['nama_orang']
                temp.kegiatan = models.Kegiatan.objects.get(id=request.POST['id_kegiatan'])
                temp.save()
                    
        elif 'delete-peserta' in request.POST:
            models.Orang.objects.get(id=request.POST['delete-peserta']).delete()

    list_kegiatan = models.Kegiatan.objects.all()
    list_all = []
    for kegiatan in list_kegiatan:
        orang = models.Orang.objects.filter(kegiatan = kegiatan)
        list_orang = []
        for i in orang:
            list_orang.append(i)
        list_all.append((kegiatan, list_orang))

    
    temp_dict = {
        'form_kegiatan' : forms.CreateKegiatanForm,
        'form_orang' : forms.CreateOrangForm,
        'data_kegiatan' : list_all
    }

    return render(request, 'home.html', temp_dict)