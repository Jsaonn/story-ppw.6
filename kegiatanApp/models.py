from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length = 30, default = "")

    def __str__(self):
        return self.nama_kegiatan


class Orang(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete = models.DO_NOTHING, null=True)
    nama_orang = models.CharField(max_length = 50)

    def __str__(self):
        return self.nama_orang
