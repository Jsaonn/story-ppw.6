from django.test import TestCase, Client
from django.urls import resolve
from .views import home
from .models import Kegiatan, Orang
from django.http import HttpRequest

# Create your tests here.

class TestStory(TestCase):

    def test_url_ada(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_app_ada(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, home)

    def test_template(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'home.html')

    def test_model_kegiatan1(self):
        Kegiatan.objects.create(nama_kegiatan='tidur')
        counter = Kegiatan.objects.all().count()
        self.assertEqual(counter, 1)
    
    def test_model_kegiatan2(self):
        Kegiatan.objects.create(nama_kegiatan='genshin')
        kegiatan = Kegiatan.objects.get(nama_kegiatan='genshin')
        Orang.objects.create(nama_orang='Jsaon', kegiatan = kegiatan)
        counter = Orang.objects.all().count()
        self.assertEqual(counter, 1)
    
    def test_relasi_model(self):
        Kegiatan.objects.create(nama_kegiatan='main')
        kegiatan = Kegiatan.objects.get(nama_kegiatan='main')
        Orang.objects.create(nama_orang='Jsaon', kegiatan=kegiatan)
        counter = Orang.objects.all().count()
        self.assertEqual(counter, 1)

    def test_model_orang(self):
        Orang.objects.create(nama_orang='jsaon')
        counter = Orang.objects.all().count()
        self.assertEqual(counter, 1)

    def test_model_nama_orang(self):
        Kegiatan.objects.create(nama_kegiatan='tidur')
        kegiatan = Kegiatan.objects.get(nama_kegiatan='tidur')
        self.assertEqual(str(kegiatan), 'tidur')

    def test_model_nama_kegiatan(self):
        Kegiatan.objects.create(nama_kegiatan='tidur')
        kegiatan = Kegiatan.objects.get(nama_kegiatan='tidur')
        self.assertEqual(str(kegiatan), 'tidur')

    def test_model_nama_kegiatan2(self):
        Kegiatan.objects.create(nama_kegiatan='tidur')
        temp = Kegiatan.objects.all()[0].id
        kegiatan = Kegiatan.objects.get(id=temp)
        Orang.objects.create(nama_orang='Jsaon', kegiatan=kegiatan)
        person = Orang.objects.get(kegiatan=kegiatan)
        self.assertEqual(str(person), 'Jsaon')

    def test_view_ada(self):
        Kegiatan.objects.create(nama_kegiatan='tidur')
        request = HttpRequest()
        response = home(request)
        html = response.content.decode('utf8')
        self.assertIn('tidur', html)

    def test_view_orang_ada(self):
        Kegiatan.objects.create(nama_kegiatan='tidur')
        temp = Kegiatan.objects.all()[0].id
        kegiatan = Kegiatan.objects.get(id=temp)
        Orang.objects.create(nama_orang='Jsaon', kegiatan=kegiatan)
        request = HttpRequest()
        response = home(request)
        html = response.content.decode('utf8')
        self.assertIn('Jsaon', html)

    def test_POST_tersimpan(self):
        counter = Orang.objects.all().count()
        response = self.client.post('/kegiatan/', temp={'nama_kegiatan' : 'tidur'})
        self.assertEqual(Kegiatan.objects.all().count(), counter)
        self.assertEqual(response.status_code,200)

    def test_delete_request(self):
        Kegiatan.objects.create(nama_kegiatan='tidur')
        temp = Kegiatan.objects.all()[0].id
        kegiatan = Kegiatan.objects.get(id=temp)
        Orang.objects.create(nama_orang='Jsaon', kegiatan=kegiatan)
        response = Client().post('/kegiatan/', temp={'delete-peserta':1})
        self.assertEqual(response.status_code,200)

    def test_landing_page(self):
        self.assertIsNotNone(home)

    